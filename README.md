[![pipeline status](https://gitlab.com/grunlab/aria2/badges/main/pipeline.svg)](https://gitlab.com/grunlab/aria2/-/commits/main)

# GrunLab Aria2

Aria2 and AriaNG non-root container images and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/aria2.md
- https://docs.grunlab.net/apps/aria2.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm-v7

[base-image]: <https://gitlab.com/grunlab/base-image>

